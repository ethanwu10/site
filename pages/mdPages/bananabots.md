---
slug: /projects/bananabots
title: BananaBots Robotics Club

outline: false
---

BananaBots is a robotics club at my local 4H, where I teach members (currently
mostly in 9th grade) robotics and programming. Club members have competed in
various competitions, including advancing to states in First LEGO League and one
team advancing to internationals in RoboCup Junior. The club also won the NSDAR
Patriot award for their community service.

[BananaBots site][bananabots-site]

[bananabots-site]: http://bananabots.club
