---
slug: /projects/elinux
title: Docker for Embedded

outline: false
---

When developing software for embedded systems, building libraries (such as
OpenCV) can be quite slow, leading to an often-times out-of-date version of a
library being used on the device. This of course leads to other issues in
development stemming from differing versions of a library in the test
environment (someone's laptop) and the production environment (a Pi on a
robot). Docker has already been used to solve similar problems on the cloud
scale; why not use it for embedded?

The folks at [Balena][balena] have already done great work for IoT devices,
and I built upon their work to create base container images containing common
robotics software packages such as OpenCV and ROS. I presented my work at the
World Maker Faire in New York City in 2018.

[balena]: https://balena.io/
