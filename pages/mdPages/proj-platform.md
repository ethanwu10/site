---
slug: /projects/platform/
title: Maze platform

outline: true
---

# Overview

Building off of my work in [Rescue Maze][rcjb], I developed a robotics platform
utilizing the same custom-designed PCB to facilitate easy entry into robotics
hardware, allowing one to focus on software. By having a powerful
microcontroller, there is sufficient processing power available to use modern
C++ programming practices with plenty of memory and CPU time to spare. In
addition, many sensors are handled without core involvement, allowing the
programmer even more processing time to utilize.

[rcjb]: https://thebeings.netlify.com

# Comparison - Pololu Kits

## Table

|                        | Platform                     | Pololu Zumo                | Pololu Balboa              |
| ---------------------- | ---------------------------- | -------------------------- | -------------------------- |
| Processor              | STM32F4                      | ATMega32u4                 | ATMega32u4                 |
| Motor driver           | On-board                     | On-board                   | On-board                   |
| Motor driver current\* | 1A / 3A                      | 1.7A / 1.8A                | 1.7A / 1.8A                |
| Encoders               | On-board                     | On-board                   | On-board                   |
| Distance Sensors       | 6 Optional ToF Lasers        | 3 IR Reflectance           | None                       |
| Light sensor           | 1 Red-Light Reflectance      | 5 IR Reflectance           | None                       |
| AHRS                   | 6-Axis Optional              | 9-Axis On-Board            | 9-Axis On-Board            |
| Free IO Pins           | 20                           | 0                          | 8                          |
| Display                | Optional Graphic OLED 128x64 | Optional Character LCD 8x2 | Optional Character LCD 8x2 |
| I2C Buses              | 1                            | 1                          | 1                          |
| SPI Buses              | 1                            | 1                          | 1                          |
| Serial Bus             | 1                            | 0                          | 0                          |
| Power                  | 1 / 2 Cell LiPo              | 6x AA                      | 6x AA                      |
| Battery Sense          | Yes                          | Yes                        | Yes                        |
| Operating Voltage      | 3.3V                         | 5V                         | 5V                         |

\*: Current shown is per channel, formatted as Continuous / Peak

## In depth

### Processor features

The board uses a powerful Cortex-M4F processor with on-board FPU (floating
point unit) and many hardware "peripherals" that make your code execute more
efficiently. The FPU, as its name suggests, accelerates floating-point math,
making running complex mathematical algorithms (such as those for accurately
tracking a robot's orientation) in real time easy. The hardware peripherals
allow for various functions to be performed without the main core (what is
running your code) needing to be actively involved. For example, the hardware
timers can generate PWM (much like `analogWrite` on Arduino, but with
flexible frequencies), capture pulses (like Arduino's `pulseIn` but running
continuously in the background), and capture PWM (giving you the duty cycle and
period of an incoming PWM signal).

### Encoders

Encoders are a common feature on robots, and it is important that they are
implemented to both be accurate and have a low resource usage. While both this
platform and the Zumo have built-in encoders, since the Zumo uses an ATMega32u4
(comparable to the ATMega328p used in the Ardunio Uno), the encoders cannot be
handled as efficiently as on more powerful processors such as the STM32 on this
board or the SAM on the Arduino Due, which are able to use hardware timers to
handle encoder counting. This saves CPU time by essentially running it in the
background, available for your program to read.

# Planned Tiers

## Base

Consists of only the populated circuit board and chassis plate for mounting
Pololu treads. Does not contain any external sensors (sensors not on the circuit
board - in other words, only contains encoders and floor reflectance sensor)

**Capabilities**

- Basic navigation using on-board encoders

## With sensors

Contains everything from base tier with external sensors and mounting hardware,
optimized for maze navigation

Sensors:
- 6x Time of Flight distance sensor
- 1x Ultrasonic sensor
- 1x 6DoF IMU

**Capabilities**

- Full maze navigation including position correction
- Low resolution area mapping

## Full

Contains everything from previous tier and a NanoPi Air with mounting hardware
for a laptop webcam

**Capabilities**

- Image processing tasks such as letter and landmark recognition
- Offload intensive processing to the Pi
