---
slug: /projects/bakeboard
title: PCB Baking Workshop

outline: false
---

In the spirit of the popular "Learn to Solder" workshops at Maker Faire, I
created a workshop for my local 4H's "Science-sational Day" leading young
children through assembling a circuit board using reflow soldering while getting
them inspired about electronics with something that they could take home. I
designed a simple badge circuit board to be assembled in the workshop, and
instructed other students on how to lead the workshop.

![The completed badge][img]

Sources for the project can be found on my GitLab [here][board] and
[here][code].

[board]: https://gitlab.com/ethanwu10/pcbbadge
[code]: https://gitlab.com/ethanwu10/pcbbadge-code
[img]: board.jpg
