---
slug: /about/
title: About Me

outline: false
---

I am an 11<sup>th</sup> grader at the Bridgewater-Raritain High School and
have been attending Storming Robots for 6 years. At a very young age, I have
been interested in robotics and electronics, having constructed a
motion-activated nightlight at age 6. From there, I have worked on various
different robotics projects, starting with LEGO NXTs and the FLL robotics
competition. Around age 10, I started working with more advanced programming,
moving from LEGO's basic drag-and-drop software to RobotC, programming in C
for NXT's, and to creating a hybrid robot comprised of an NXT as a master and
an Arduino as a sensor slave (programmed in C++) communicating to each other
via I<sup>2</sup>C. I have also created Android apps to interact with robots
over Bluetooth for remote control capabilities. In my more recent projects, I
have designed PCBs for robotics, and developed tooling for deploying software
for robots. I enjoy doing just about anything computers-related, including
web design, algorithms programming, CGI art, and Linux.

I am very self-motivated, not afraid to learn new things in order to get the
job done, and done right. I have taught myself all about computers and
programming, including C/C++, Javascript, Java, Python, Linux, KiCAD,
FreeCAD, Blender, and OpenCV.
