---
slug: /projects/nactf
title: NACTF

outline: false
---

![NACTF logo](NACTF.png)

[NACTF][nactf] is a Capture The Flag (CTF) cybersecurity competition for
middle and high school students, entirely run by 2 of my friends and I. The
competition had a prize pool of $800, ran for 5 days, and attracted over 1300
participants.

I was responsible for administrating the challenge servers that all of the
participants would connect to. The challenges ran in a Kubernetes cluster,
with configuration available on our [GitLab repositories][gitlab]. In
addition, I was a challenge author (some of which are available
[here][problems]).

[nactf]: https://nactf.com
[gitlab]: https://gitlab.com/nactf
[problems]: https://gitlab.com/nactf/challenges-2019
