import React from 'react'
import styled from 'styled-components'
import rehypeReact from 'rehype-react'
import { Link as AnchorLink } from 'react-scroll'
import { Link as InternalLink } from 'gatsby'

const Table = styled(({ children, ...props }) =>
  <div {...props}>
    <table>
      {children}
    </table>
  </div>
)`
  width: 100%;
  overflow-x: auto;
`

const externalLinkRegex = /^(https?:)?\/\//
const staticLinkRegex = /^\/static\//

const MdLink = ({ href, ...props }) =>
  href[0] === '#' ? (
    <AnchorLink href={href} to={href.substring(1)} {...props} smooth={true} />
  ) : externalLinkRegex.test(href) || staticLinkRegex.test(href) ? (
    <a href={href} {...props} />
  ) : (
    <InternalLink to={href} {...props} />
  )

const HeadingHref = styled.a`
  color: black;
  font-size: 90%;
  text-decoration: none;
  margin-left: 0.3em;

  opacity: 0;
  h1:hover &, h2:hover &, h3:hover &, h4:hover &, h5:hover &, h6:hover & {
    opacity: 0.2;
  }
  &&:hover {
    opacity: 0.4;
  }
  transition: opacity 200ms;
`

const makeHeading = (Heading) => ({ id, children, ...props }) =>
  <Heading id={id} {...props}>
    {children}
    <HeadingHref href={`#${id}`} className="heading-href">#</HeadingHref>
  </Heading>

const headingComponents = {};

for (let heading of ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']) {
  headingComponents[heading] = makeHeading(heading)
}

function rehype2React(options) {
  rehypeReact.call(this, {
    createElement: React.createElement,
    components: {
      'table': Table,
      'a': MdLink,
      ...headingComponents
    },
    ...options
  })
}

export default rehype2React
