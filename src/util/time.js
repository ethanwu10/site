import moment from 'moment'

const absoluteTime = (time) =>
  moment(time).format('MMMM D, YYYY')

const relativeTime = (time, capitalize) => {
  capitalize = capitalize || false
  time = moment(time)
  let text = ''
  if (time.isAfter(moment().subtract(1, 'days'))) {
    text = 'today'
  } else {
    text = time.fromNow()
  }
  if (capitalize) {
    return text.charAt(0).toLocaleUpperCase() + text.slice(1)
  } else {
    return text
  }
}

const shouldUseRelativeTime = (time) =>
  moment(time).isAfter(moment().subtract(1, 'weeks'))

const autoTime = (time, capitalize) => {
  time = moment(time)
  if (shouldUseRelativeTime(time)) {
    return relativeTime(time, capitalize)
  } else {
    return absoluteTime(time)
  }
}

export { relativeTime, absoluteTime, shouldUseRelativeTime, autoTime }
