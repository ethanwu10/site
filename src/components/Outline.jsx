import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Link } from 'react-scroll'
import { IoMdList } from 'react-icons/io'

// const OutlineItem = ({ text, slug, active, className, children }) =>
//   <li className={className + (active ? ' active': '')}key={slug}>
//     <a href={`#${slug}`}>{text}</a>
//     {children}
//   </li>

// OutlineItem.propTypes = {
//   text: PropTypes.string.isRequired,
//   slug: PropTypes.string.isRequired,
//   active: PropTypes.bool,
//   className: PropTypes.string,
//   children: PropTypes.node
// }

// OutlineItem.defaultProps = {
//   active: false,
//   className: ''
// }

const HrefLink = ({ to, ...props }) =>
  <Link to={to} href={`#${to}`} {...props} />

HrefLink.propTypes = Link.propTypes

HrefLink.defaultProps = {
  smooth: true,
  duration: 500
}

const OutlineLink = styled(HrefLink)`
  color: inherit;
  text-decoration: none;

  :focus {
    outline: none;
  }
`

const OutlineIcon = styled(IoMdList)`
  font-size: 2.5rem;
  /*
  @media not all and (min-width: 640px) {
    display: none;
  }
  */
`

const OutlineWrapper = styled.nav`
@media print {
  display: none;
}

ul {
  margin: 0;
  list-style: none;
  position: relative;

  ::before { /* FIXME - Deduplicate */
    content: '';
    display: block;

    width: 1px;
    height: 100%;
    left: 0;
    top: 0;
    position: absolute;

    background-color: #000; /* FIXME */
  }
}
li {
  margin: 0;
  padding-left: 0.6em;
  margin: 6px 0;
  position: relative;

  ::before {
    content: '';
    display: block;

    width: 1px;
    height: 100%;
    left: 0;
    top: 0;
    position: absolute;

    transform-origin: center left;
    transform: scaleX(1);

    background-color: #000; /* FIXME */

    transition: .5s all;
  }
}
li.active::before, li.subactive::before, li:hover::before {
  transform: scaleX(2);
}
`

const Outline = ({ headings, activeSlug, activeClass, subActiveClass, spy }) => {
  const [currActive, setCurrActive] = useState(activeSlug);
  let currLevel = 1
  let currParent = {
    text: '',
    slug: '',
    parent: null,
    children: [],
    active: false,
    subActive: false
  }
  for (let heading of headings) {
    if (heading.level > currLevel) {
      // Descend
      if (heading.level > currLevel + 1) {
        // Do not allow nonconsecutive sublevels
        throw new Error(`Missing subsection: level ${currLevel + 1}`)
      }
      currParent = currParent.children[currParent.children.length - 1]
      currLevel++;
    }
    while (heading.level < currLevel) {
      currParent = currParent.parent
      currLevel--;
    }
    const isActive = heading.slug === currActive;
    currParent.children.push({
      text: heading.text,
      slug: heading.slug,
      parent: currParent,
      children: [],
      active: isActive,
      subActive: false
    })
    if (isActive) {
      let parent = currParent
      while (parent !== null) {
        parent.subActive = true;
        parent = parent.parent
      }
    }
  }
  while (1 < currLevel) { // Move back up to root
    currParent = currParent.parent
    currLevel--;
  }

  function buildComponentTree(node) {
    const children = node.children.map(buildComponentTree)
    if (node.parent !== null) {
      return (
        <li
          className={node.active ? activeClass : (node.subActive ? subActiveClass : null)}
        >
          <OutlineLink
            to={node.slug}
            spy={spy} onSetActive={() => setCurrActive(node.slug)}
          >
            {node.text}
          </OutlineLink>
          {children.length !== 0
          ? <ul>{children}</ul>
          : null
          }
        </li>
      )
    } else {
      return children
    }
  }

  return (
    <OutlineWrapper>
      <OutlineIcon />
      <ul>
        {buildComponentTree(currParent)}
      </ul>
    </OutlineWrapper>
  )
}

Outline.propTypes = {
  headings: PropTypes.arrayOf(PropTypes.shape({
    level: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired
  })),
  activeSlug: PropTypes.string,
  activeClass: PropTypes.string,
  subActiveClass: PropTypes.string,
  spy: PropTypes.bool
}

Outline.defaultProps = {
  activeClass: 'active',
  subActiveClass: 'subactive',
  spy: false
}

export default Outline
