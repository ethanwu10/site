import React, { useState } from 'react'
import styled, { css } from 'styled-components'
import theme, { makeGradient } from '../../config/theme'
import { FaChevronLeft, FaChevronRight } from 'react-icons/fa'
import { IoMdList } from 'react-icons/io'

const contentHorizPad = '2rem'

const Container = styled.div`
  display: flex;

`

const Clipper = styled.div`
  overflow: hidden;
  flex: 1 0;
`

const siderailPadding = `1rem`

const Siderail = styled.div`
  background-color: #eee /*${theme.colors.light.muted.color}*/; /* FIXME */

  & > * {
    /* Work around Firefox not respecting bottom padding when scrolling */
    padding: ${siderailPadding};
    padding-bottom: 0;
    & > :last-child {
      margin-bottom: ${siderailPadding};
    }
  }

  @media (min-width: 640px) {
    flex: 0 0 15rem;
    margin: -5rem 0;
    padding: calc(5rem + 1.5rem) 0;

    & > * {
      position: sticky;
      top: 0;
      bottom: 0;

      height: auto;
      max-height: 100vh;
      overflow-y: auto;
    }
  }
  @media not all and (min-width: 640px) {
    position: fixed;
    top: 0;
    left: 0;
    height: 100vh;
    width: 15rem;
    box-shadow: 0 0 2rem rgba(0,0,0,0.3);

    opacity: 0;
    transform: translateX(-100%);
    transition: all 0.3s;
    z-index: 500;

    ${({visible}) => visible && css`
      opacity: 1;
      transform: translateX(0);
    `}
  }
`

const codeBorderRadius = '0.5rem';

const Content = styled.article`
  max-width: 960px;
  margin: 1.5rem auto 1.5rem;
  padding: 2rem ${contentHorizPad} 1rem;

  pre[class*="language-"] {
    margin-left:  calc(-${contentHorizPad} - ${codeBorderRadius});
    margin-right: calc(-${contentHorizPad} - ${codeBorderRadius});
    padding-left:  0;
    padding-right: 0;

    border-radius: ${codeBorderRadius};
  }

  pre code[class*="language-"] {
    display: inline-block;
    margin-left:  calc(${contentHorizPad} + ${codeBorderRadius});
    margin-right: calc(${contentHorizPad} + ${codeBorderRadius});
  }

  *:not(pre) > code {
    padding: 0.1em 0.2em;
    border-radius: 0.2em;
    background-color: #eee /*${theme.colors.light.muted.color}*/; /* FIXME */
    box-decoration-break: clone;
  }

  a:not(.gatsby-resp-image-link):not(.heading-href) {
    background-color: rgba(58, 185, 255, 0.1);
    color: rgb(22, 153, 225);
    text-decoration: none;
    border-bottom: rgb(22, 153, 225) 1px solid;

    margin: -0.00em -0.05em;
    padding: 0.00em  0.05em;

    transition: all 0.3s;

    :hover {
      background-color: rgba(58, 185, 255, 0.2);
      border-bottom-width: 2px;
    }
  }

  blockquote {
    border-left: rgba(0, 0, 0, 0.1) 0.25em solid;
    margin-left: 0;
    padding-left: 1em;
  }
`

const SiderailToggleButton = styled((props) =>
  <div {...props}>
    {/* <FaChevronLeft /> <FaChevronRight /> */}
    <IoMdList />
  </div>
)`
  @media (min-width: 640px) {
    display: none;
  }
  position: fixed;
  bottom: 1.5rem;
  right: 1.5rem;
  background: ${makeGradient(theme.colors.primary, '135deg')};
  height: 3rem;
  width: 3rem;
  border-radius: 999rem;
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 0 0.2rem 1rem rgba(0,0,0,0.3);
  z-index: 999;
  font-size: 1.5rem;

  transform: translateY(0);
  transition: all 0.3s;
  &:hover {
    transform: translateY(-10%) scale(1.05);
    box-shadow: 0 0.5rem 1.2rem rgba(0,0,0,0.3);
  }

  color: ${theme.colors.light.color};
`

const SiderailCloseBox = styled.div`
  @media (min-width: 640px) {
    display: none;
  }
  position: absolute;
  top: 0;
  right: 0;
  width: 999em;
  transform: translateX(100%);
  height: 100%;
  ${({visible}) => visible || css`
    display: none;
  `}
`

const ContentWrapper = ({
  siderail,
  children
}) => {
  let content, side
  if (siderail) {
    [content, side] = children
  } else {
    content = children
  }
  const [siderailVisible, setSiderailVisible] = useState(false);
  return (
    <Container>
      {
        siderail &&
        <>
          <Siderail visible={siderailVisible}>
            {side}
            <SiderailCloseBox
              visible={siderailVisible}
              onClick={() => setSiderailVisible(false)} />
          </Siderail>
          <SiderailToggleButton onClick={() => setSiderailVisible(!siderailVisible)} />
        </>
      }
      <Clipper>
        <Content>
          {content}
        </Content>
      </Clipper>
    </Container>
  )
}

export default ContentWrapper

export { contentHorizPad }
