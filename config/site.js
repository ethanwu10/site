module.exports = {
  pathPrefix: '/', // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "/portfolio"

  siteTitle: 'Ethan Wu', // Navigation and Site Title
  siteTitleAlt: '', // Alternative Site title for SEO
  siteTitleShort: '', // short_name for manifest
  siteHeadline: '', // Headline for schema.org JSONLD
  siteUrl: 'https://ethanwu.dev', // Domain of your site. No trailing slash!
  siteLanguage: 'en', // Language Tag on <html> element
  siteLogo: '/logo.png', // Used for SEO and manifest
  siteDescription: '',
  author: '', // Author for schema.org JSONLD

  // Manifest and Progress color
  themeColor: '#4a94e8',
  backgroundColor: '#324459',
}
