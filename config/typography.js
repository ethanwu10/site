import theme from './theme'
import Typography from 'typography'

const typography = new Typography({
  baseFontSize: theme.typography.baseSize,
  baseLineHeight: theme.typography.lineHeight,
  headerFontFamily: theme.typography.headerFonts,
  bodyFontFamily: theme.typography.bodyFonts,
})

export default typography
