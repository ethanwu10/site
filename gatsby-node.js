const path = require('path')
const _ = require('lodash')
const { createFilePath } = require('gatsby-source-filesystem')

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  return new Promise((resolve, reject) => {
    const blogListTemplate = path.resolve('src/templates/blogList.jsx')
    const blogPostTemplate = path.resolve('src/templates/blogPost.jsx')

    const mdPageTemplate = path.resolve('src/templates/mdPage.jsx')

    const presentationTemplate = path.resolve('src/templates/presentation.jsx')

    graphql(`
      query {
        blog: allMarkdownRemark(
          sort: { order: DESC, fields: [frontmatter___date, fields___slug] }
          filter: {
            fields: {
              sourceInstanceName: { eq: "blog" }
            }
          }
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                draft
                outline
              }
            }
          }
        }
        mdPages: allMarkdownRemark(
          filter: {
            fields: {
              sourceInstanceName: { eq: "mdPages" }
            }
          }
        ) {
          edges {
            node {
              frontmatter {
                slug
                outline
              }
            }
          }
        }
        presentations: allPresentationsYaml {
          edges {
            node {
              slug
            }
          }
        }
      }
    `).then(result => {
      if (result.errors) {
        return reject(result.errors)
      }

      const showDrafts = process.env.NODE_ENV !== 'production'

      const blogPosts = result.data.blog.edges
      const publishedPosts = blogPosts.filter(({ node }) => !node.frontmatter.draft)
      const visiblePosts = showDrafts ? blogPosts : publishedPosts

      const blogPostsPerPage = 10

      const numPages = Math.ceil(visiblePosts.length / blogPostsPerPage)

      for (const i of _.range(numPages)) {
        createPage({
          path: i === 0 ? `/blog` : `/blog/page/${i + 1}`,
          component: blogListTemplate,
          context: {
            pageNum: i + 1,
            numPages,
            limit: blogPostsPerPage,
            skip: i * blogPostsPerPage,
            draftFilter: showDrafts ? {} : {"eq": false}
          }
        })
      }

      blogPosts.map(({ node }) => {
        createPage({
          path: node.fields.slug,
          component: blogPostTemplate,
          context: {
            slug: node.fields.slug,
            outline: node.frontmatter.outline || false
          }
        })
      })

      const mdPages = result.data.mdPages.edges
      // filtering out empty slugs prevents Gatsby from crashing
      // when an empty file is created
      mdPages.filter(({ node }) => node.frontmatter.slug)
      .map(({ node }) => {
        createPage({
          path: node.frontmatter.slug,
          component: mdPageTemplate,
          context: {
            slug: node.frontmatter.slug,
            outline: node.frontmatter.outline
          }
        })
      })

      const presentations = result.data.presentations.edges
      presentations.map(({ node }) => {
        createPage({
          path: `/presentations/${node.slug}`,
          component: presentationTemplate,
          context: {
            slug: node.slug
          }
        })
      })

      return resolve()
    })
  })
}

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  if (node.internal.type === 'MarkdownRemark') {
    const sourceNode = getNode(node.parent)
    let sourceInstanceName = 'unknown'
    if (sourceNode.internal.type === 'File') {
      sourceInstanceName = sourceNode.sourceInstanceName
    }

    createNodeField({
      node,
      name: `sourceInstanceName`,
      value: sourceInstanceName
    })

    if (sourceInstanceName === 'blog') {
      let slug = createFilePath({node, getNode, basePath: `pages/blog`})
      slug = slug.replace(/\/(\d{4})-(\d{2})-(\d{2})-/, (_, a, b, c) => `/${a}/${b}/${c}/`)
      createNodeField({
        node,
        name: 'slug',
        value: `/blog${slug}`,
      })
    }
  }
}

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions
  const typeDefs = `
    type ProjectsYaml implements Node {
      title: String!
      detail: String
      desc: String
      to: String
      sortPriority: Int
      bgImage: File @link(by: "relativePath") # TODO: match sourceInstanceName eq "img"
    }
    type PresentationsYaml implements Node {
      slug: String!
      title: String!
      date: String!
      sortPriority: Int
      eventTitle: String
      videoId: String
      slideDeck: String
      paperUrl: String
    }
  `

  createTypes(typeDefs)
}
