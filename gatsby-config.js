const path = require('path')
const config = require('./config/site')
const theme = require('./config/theme')
const Color = require('color')

module.exports = {
  // Metadata
  siteMetadata: {
    siteTitle: config.siteTitle,
    siteTitleAlt: config.siteTitleAlt,
    siteTitleShort: config.siteTitleShort,
    siteDescription: config.siteDescription,
    siteUrl: config.siteUrl,
    siteLogo: config.siteLogo,
  },

  // Plugins
  plugins: [
    {
      resolve: `gatsby-plugin-alias-imports`,
      options: {
        alias: {
          '@components': path.resolve(__dirname, 'src/components'),
          '@config': path.resolve(__dirname, 'config')
        }
      }
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: 'gatsby-plugin-styled-components',
      options: {
        displayName: process.env.NODE_ENV !== 'production',
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `img`,
        path: path.resolve(__dirname, 'src/img'),
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `blog`,
        path: path.resolve(__dirname, 'pages/blog'),
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `mdPages`,
        path: path.resolve(__dirname, 'pages/mdPages'),
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: path.resolve(__dirname, `data`),
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          `gatsby-remark-smartypants`,
          {
            resolve: `gatsby-remark-external-links`,
            options: {
              target: `_blank`,
              rel: `nofollow noopener noreferrer`,
            }
          },
          `gatsby-remark-images`,
          {
            resolve: `gatsby-remark-prismjs`,
            options: {
              aliases: {
                sh: 'bash',
              },
              noInlineHighlight: true, // Dark theme looks bad inline
            }
          },
        ],
      },
    },
    `gatsby-transformer-yaml`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `config/typography`
      }
    },
    {
      resolve: `gatsby-plugin-nprogress`,
      options: {
        color: Color(theme.colors.secondary.color).lightness(75).saturationv(80).hex(),
        showSpinner: false
      }
    },
    `gatsby-plugin-catch-links`,
    `gatsby-plugin-lodash`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: config.siteTitle,
        short_name: config.siteTitleShort,
        lang: config.siteLanguage,
        description: config.siteDescription,
        start_url: '/',
        background_color: config.backgroundColor,
        theme_color: config.themeColor,
        display: 'standalone',
      }
    },

    // Must be last
    `gatsby-plugin-offline`,
    `gatsby-plugin-netlify`,
    `gatsby-plugin-netlify-cache`
  ]
}
