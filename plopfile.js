/**
 * Input validator - ensure input is not empty.
 *
 * @param {string} name
 * @return {boolean|string}
 */
const inputRequired = name => {
  return value => (/.+/.test(value) ? true : `${name} is required`);
};

module.exports = (plop) => {
  plop.setGenerator('blog-post', {
    description: 'Create a blog post',
    prompts: [
      {
        type: 'input',
        name: 'title',
        message: 'Post title:',
        validate: inputRequired('Title')
      },
      {
        type: 'input',
        name: 'tags',
        message: 'Tags (comma-separated):'
      },
      {
        type: 'confirm',
        name: 'draft',
        message: 'Draft?',
        default: true,
      }
    ],
    actions: data => {
      // Get current date
      data.date = new Date().toISOString()
      data.dateSlug = data.date.split('T')[0]

      if (data.tags) {
        // Split into YAML array
        data.tags = `tags:\n  - ${data.tags.split(',').join('\n  - ')}`
      }

      return [{
        type: 'add',
        path: 'pages/blog/{{dateSlug}}-{{dashCase title}}.md',
        templateFile: 'plop-templates/blogPost.hbs',
      }]
    }
  })
  plop.setGenerator('md-page', {
    description: 'Create Markdown page',
    prompts: [
      {
        type: 'input',
        name: 'slug',
        message: 'Page path:',
        validate: inputRequired('Path')
      },
      {
        type: 'input',
        name: 'title',
        message: 'Page title:',
        validate: inputRequired('Title')
      },
      {
        type: 'confirm',
        name: 'outline',
        message: 'Generate outline?',
        default: false
      },
      {
        type: 'input',
        name: 'file',
        message: 'File name:',
        validate: inputRequired('Path')
      }
    ],
    actions: [{
        type: 'add',
        path: 'pages/mdPages/{{file}}',
        templateFile: 'plop-templates/mdPage.hbs'
      }]
  })
}
